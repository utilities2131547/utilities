from typing import Self
from threading import Lock


class Singleton:
    _instances = {}
    _lock = Lock()

    def __new__(cls, *args, **kwargs) -> Self:
        with cls._lock:
            if cls not in cls._instances:
                instance = super().__new__(cls)
                cls._instances[cls] = instance
        return cls._instances[cls]