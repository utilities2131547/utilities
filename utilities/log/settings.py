from pydantic_settings import BaseSettings


class LogSettings(BaseSettings):
    LOG_LEVEL: str = "INFO"