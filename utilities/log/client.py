import logging
from rich.logging import RichHandler

from utilities.settings import Settings



def get_logger(name: str = None):
    settings = Settings()
    
    logger = logging.getLogger(name)
    logger.setLevel(settings.LOG_LEVEL)
    logger.addHandler(RichHandler(rich_tracebacks=True))
    return logger
