from typing import Dict
from sqlalchemy.orm import Session
from sqlalchemy.engine import create_engine, Engine
from sqlalchemy.exc import OperationalError

from utilities.settings import Settings
from utilities.abstract import Singleton
from utilities.log.client import get_logger


logger = get_logger(__name__)


def get_engine(db_engine: str, settings: Settings, connect_args: Dict) -> Engine:
    driver = {
        "mysql": "mysql+pymysql",
        "postgres": "postgresql+psycopg2",
    }.get(db_engine)
    db_uri = f"{driver}://{settings.DB_USER}:{settings.DB_PASSWORD}@{settings.DB_HOST}:{settings.DB_PORT}/{settings.DB_NAME}"
    engine = create_engine(db_uri, connect_args=connect_args)
    return engine


class SqlOrmClient(Singleton):
    _inited = False

    def __init__(self, db_engine: str = "postgres", connect_args: Dict = {}) -> None:
        settings = Settings()
        self.engine = get_engine(db_engine, settings, connect_args)
        if not SqlOrmClient._inited:
            logger.info(f"Connecting to {db_engine} at {settings.DB_HOST}:{settings.DB_PORT}/{settings.DB_NAME} as {settings.DB_USER}")
            try:
                connection = self.engine.connect()
            except OperationalError as e:
                logger.error(e, exc_info=True)
            else:
                logger.info("Connected!")

        SqlOrmClient._inited = True

    def get_session(self) -> Session:
        session = Session(self.engine)
        return session
        

