from pydantic_settings import BaseSettings


class SqlSettings(BaseSettings):
    DB_HOST: str = "localhost"
    DB_PORT: int = 5432
    DB_USER: str = "admin"
    DB_PASSWORD: str = "admin"
    DB_NAME: str = "admin"