from pydantic_settings import BaseSettings


class RedisSettings(BaseSettings):
    REDIS_HOST: str = "localhost"
    REDIS_PORT: int = 6379
    REDIS_DB: int = 0
    REDIS_USER: str = "default"
    REDIS_PASSWORD: str = ""