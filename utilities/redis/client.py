from typing import Self
from redis import Redis
from redis.exceptions import ConnectionError, AuthenticationError

from utilities.settings import Settings
from utilities.abstract import Singleton
from utilities.log.client import get_logger


logger = get_logger(__name__)


class RedisClient(Singleton):
    _inited = False

    def __init__(self, *args, **kwargs) -> None:
        settings = Settings()
        self.client = Redis.from_url(f"redis://{settings.REDIS_USER}:{settings.REDIS_PASSWORD}@{settings.REDIS_HOST}:{settings.REDIS_PORT}/{settings.REDIS_DB}")
        if not RedisClient._inited:
            logger.info(f"Connecting to Redis at {settings.REDIS_HOST}:{settings.REDIS_PORT} as {settings.REDIS_USER}")
            try:
                self.client.ping()
            except AuthenticationError as e:
                logger.error(e, exc_info=True)
            except ConnectionError as e:
                logger.error(e, exc_info=True)
            except Exception as e:
                logger.error(e, exc_info=True)
            else:
                logger.info("Connected!")
        RedisClient._inited = True
        

    def get_client(self) -> Redis:
        return self.client