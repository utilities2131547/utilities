from pydantic_settings import BaseSettings

from utilities.abstract.singleton import Singleton
from utilities.redis.settings import RedisSettings
from utilities.sql.settings import SqlSettings
from utilities.log.settings import LogSettings


class Settings(Singleton, LogSettings, RedisSettings, SqlSettings, BaseSettings):
    pass
